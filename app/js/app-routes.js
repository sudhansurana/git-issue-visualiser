(function () {
  'use strict';

  angular
    .module('visualiser')
    .config(config);

  function config($routeProvider) {
    $routeProvider.when('/main',
	   {
	       templateUrl: 'templates/main.html',
	       controller: 'MainCtrl',
	       controllerAs: 'main'
	   }
	);
	$routeProvider.otherwise({redirectTo: '/main'});
  }
}());
