(function () {
  'use strict';

  /* @ngdoc object
   * @name progressbar
   * @description
   *
   */
  angular
    .module('visualiser', [
      'ngRoute',
      'ngUrlParser'
    ]);
}());
