app.directive('validateGitUrl', function() {
  var GIT_REGEXP = ((git|ssh|http(s)?)|(git@[\w\.]+))(:(//)?)([\w\.@\:/\-~]+)(\.git)(/)?;
  return {
    link: function(scope, elm) {
      elm.on("keyup",function(){
            var isMatchRegex = GIT_REGEXP.test(elm.val());
            if( isMatchRegex&& elm.hasClass('warning') || elm.val() == ''){
              elm.removeClass('warning');
            }else if(isMatchRegex == false && !elm.hasClass('warning')){
              elm.addClass('warning');
            }
      });
    }
  }
});