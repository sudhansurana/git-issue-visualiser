(function () {
  'use strict';

  /**
   * @ngdoc directive
   * @name visualiser.directive:bar
   * @restrict E
   * @element
   *
   * @description
   *
   * @example
     <example module='visualiser'>
       <file name='index.html'>
        <pie-chart></pie-chart>
       </file>
     </example>
   *
   */
  angular
    .module('visualiser')
    .directive('pieChart', pieChart);

  function pieChart() {
    return {
      restrict: 'E',
      scope: {
        title: '@',
        data: '='
      },
      template: '<div></div>',
      replace: false,
      link: function (scope, element, attrs, ctrl) {
        /* jshint unused:false */
        /* eslint "no-unused-vars": [2, {"args": "none"}] */

        function dataWatcher() {
          return scope.data;
        }
        scope.$watch(dataWatcher, function() {
            scope.chart = Highcharts.chart(element[0], {
              chart: {
                  type: 'pie'
              },
              title: {
                  text: scope.title
              },
              plotOptions: {
                  pie: {
                      allowPointSelect: true,
                      cursor: 'pointer',
                      dataLabels: {
                          enabled: true,
                          format: '<b>{point.label}</b>: {point.percentage:.1f} %'
                      }
                  }
              },
              series: [{
                name: 'Issues Visualiser',
                colorByPoint: true,
                data: scope.data
              }]
          });
        });
      }
    };
  }
}());
