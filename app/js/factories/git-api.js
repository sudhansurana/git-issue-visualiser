(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name task.factory:BarLoader
   *
   * @description
   *
   */
  angular
    .module('visualiser')
    .factory('GitAPI', GitAPI);

  GitAPI.$inject = ['$http', '$q'];

  function GitAPI($http, $q) {
    var API_URI = 'https://api.github.com/',
        GitAPIBase = {
          checkURL: checkURL,
          getIssues: getIssues
        };

    return GitAPIBase;

    function checkURL(urlObj) {
      var deferred = $q.defer(),
          url;
      //update url
      url = API_URI;
      url += 'repos';
      url += urlObj.pathname.replace('.git', '');
      $http({
        method: 'GET',
        url: url
      })
      .then(function (response) {
        deferred.resolve(response.data);
      })
      .catch(function (error) {
        deferred.reject(error.message || error.data);
      });
      return deferred.promise;
    }

    function getIssues(urlObj, gitObj) {
      var deferred = $q.defer(),
          tmpArr = urlObj.pathname.replace('.git', '').split('/'),
          author = tmpArr[1],
          repo = tmpArr[2],
          url;
      //update url
      url = API_URI;
      url += 'search/issues?q=';
      url += 'author:'+author;
      url += '&repo:'+  repo;
      //url += '&is:issue&is:open';
      if (gitObj) {
        url = gitObj.issues_url;
        url = url.replace('{/number}', '');
      }
      $http({
        method: 'GET',
        url: url
      })
      .then(function (response) {
        deferred.resolve(response.data);
      })
      .catch(function (error) {
        deferred.reject(error.message || error.data);
      });
      return deferred.promise;
    }
  }
}());
