(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name visualiser.controller:NavbarCtrl
   *
   * @description
   *
   */
  angular
    .module('visualiser')
    .controller('MainCtrl', MainCtrl);

  MainCtrl.$inject = ['$log', 'GitAPI', 'urlParser'];

  function MainCtrl($log, GitAPI, urlParser) {
    var vm = this,
    	tmpPieConfig,
    	reg = /(?:git|ssh|https?|git@[\w\.]+):(?:\/\/)?[\w\.@:\/~_-]+\.git(?:\/?|\#[\d\w\.\-_]+?)$/;
    vm.ctrlName = 'main';
    vm.search = 'Please enter a github URL';
    vm.isValid = true;
    vm.errorMessage = 'Provide GIT URL is Invalid';
    vm.gitObj;
    vm.urlObj;
    vm.openedIssues = [];
    vm.closedIssues = [];
    vm.pieData = [];
    vm.pieSeries = {
		name: 'Custom Name',
		data: [],
		type: 'pie',
		id: 'pie-chart'
	};
    vm.calledOnce = false;
    vm.pieConfig = {
    	chart: {
    		type: 'pie'
    	},
    	series: [vm.pieSeries],
    	title: {
    		text: 'No of opened and closed issues'
    	}
    };
    vm.validateURL = function(url) {
    	var urlObj;
    	vm.issues = [];
    	vm.gitObj = null;
    	if (reg.test(url)) {
    		vm.isValid = true;
    	} else {
    		vm.errorMessage = 'Provide GIT URL is Invalid';
    		vm.isValid = false;
    	}
    	vm.urlObj = urlParser.parse(url);
    	GitAPI.checkURL(vm.urlObj)
    		.then(function (res) {
    			$log.log(res);
    			vm.gitObj = res;
    			getAllIssues();
    		})
    		.catch(function (error) {
    			$log.error(error);
    			vm.isValid = false;
    			vm.errorMessage = 'Privide Git URL' + error.message;
    		});
    };

    function getAllIssues() {
    	GitAPI.getIssues(vm.urlObj)
    		.then(function (res) {
    			$log.log(res);
    			vm.openedIssues = _.where(res.items || res, {state:'open', repository_url: vm.gitObj.url});
    			vm.closedIssues = _.where(res.items || res, {state:'closed', repository_url: vm.gitObj.url});
    			
    			vm.pieData = [{
	    				y: vm.openedIssues.length,
	    				name: 'Open Issues'
	    			}, {
	    				y: vm.closedIssues.length,
	    				name: 'Closed Issues'
	    			}];
    			console.log(res);
    		})
    		.catch(function (error) {
    			$log.error(error);
    		})
    	if(vm.openedIssues.length === 0 && vm.openedIssues.length === 0 ) {
	    	GitAPI.getIssues(vm.urlObj, vm.gitObj)
	    		.then(function (res) {
	    			$log.log(res);
	    			vm.openedIssues = _.where(res.items || res, {state:'open'});
	    			vm.closedIssues = _.where(res.items || res, {state:'closed'});
	    			
	    			vm.pieData = [{
	    				y: vm.openedIssues.length,
	    				name: 'Open Issues'
	    			}, {
	    				y: vm.closedIssues.length,
	    				name: 'Closed Issues'
	    			}];
    				console.log(res);
	    		})
	    		.catch(function (error) {
	    			$log.error(error);
	    		})
    	}
    }
    $log.log(vm.ctrlName);
  }
}());
