(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name visualiser.controller:NavbarCtrl
   *
   * @description
   *
   */
  angular
    .module('visualiser')
    .controller('NavbarCtrl', NavbarCtrl);

  NavbarCtrl.$inject = ['$location'];

  function NavbarCtrl($location) {
    var vm = this;
    vm.main = function() {
        $location.url('/main');
    };
  }
}());
