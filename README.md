# Github Issue Visualiser

A barebones Node.js with AngularJS.

This Application is meant to be deployed onto and run on [Heroku](https://www.heroku.com/).

Check it out live [Here](https://github-issue-visualiser.herokuapp.com/)

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) and the [Heroku Toolbelt](https://toolbelt.heroku.com/) installed.

1. Git bash at the directory.
2. Type "npm install"
3. Type "heroku local"

Your app should now be running on [localhost:5000](http://localhost:5000/).

## Deploying to Heroku

```
$ heroku create
$ git push heroku master
$ heroku open
```
or

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)
